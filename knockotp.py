#!/usr/bin/env python3

import argparse
import hashlib
import hmac
import ipaddress
import os
import socket
import time


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	af = parser.add_mutually_exclusive_group()
	af.add_argument('-4', '--ipv4', action='store_true', help="use IPv4 to knock")
	af.add_argument('-6', '--ipv6', action='store_true', help="use IPv6 to knock")
	parser.add_argument('-i', '--interval', metavar="DURATION", type=int, help="duration of an OTP in seconds (60)", default=60)
	parser.add_argument('-s', '--secret', metavar="FILE", type=str, help="path to secret file (~/.config/knockotp/secret)", default=os.path.expanduser('~/.config/knockotp/secret'))
	parser.add_argument('-a', '--algorithm', metavar="ALG", type=str, help="hash algorithm to use (sha3_512)", default='sha3_512')
	parser.add_argument('-r', '--retry', type=int, help="number of UDP packet to send (1)", default=1)
	parser.add_argument('-w', '--wait', type=float, help="number of seconds to wait between retries", default=1.0)
	parser.add_argument('-p', '--port', type=int, help="destination UDP port (22)", default=22)
	parser.add_argument('host', type=str, help="destination host")
	args = parser.parse_args()

	assert args.interval > 0
	assert args.retry > 0
	assert args.wait > 0.0

	assert os.path.isfile(args.secret)
	with open(args.secret, 'rb') as secret_file:
		secret = secret_file.read()

	assert hasattr(hashlib, args.algorithm)
	algorithm = getattr(hashlib, args.algorithm)

	message = int(time.time()) // args.interval
	message = message.to_bytes((message.bit_length() + 7) // 8, 'little')
	otp = hmac.HMAC(secret, message, digestmod=algorithm)

	if args.ipv4:
		af = socket.AF_INET
	elif args.ipv6:
		af = socket.AF_INET6
	else:
		af = 0

	addrinfo = socket.getaddrinfo(args.host, 22, af)

	if not af:
		af = addrinfo[0][0]
	sock = socket.socket(af, socket.SOCK_DGRAM)
	sock.sendto(otp.digest(), (addrinfo[0][4][0], args.port))
	for _ in range(1, args.retry):
		time.sleep(args.wait)
		sock.sendto(otp.digest(), (addrinfo[0][4][0], args.port))
