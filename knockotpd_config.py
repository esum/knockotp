import hashlib


interval = 60
overlap = 10
algorithm = hashlib.sha3_512
# List secrets of type bytes here
secrets = [
]

nft = ['/usr/bin/nft']
nft_table_type = 'inet'
nft_table_name = 'filter'
nft_table_set = 'otp'
