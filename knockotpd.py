#!/usr/bin/env python3 -B

import base64
import hashlib
import hmac
import subprocess
import time

import knockotpd_config


if __name__ == '__main__':
	message = int(time.time()) // knockotpd_config.interval
	message = message.to_bytes((message.bit_length() + 7) // 8, 'little')
	for secret in knockotpd_config.secrets:
		otp = hmac.HMAC(secret, message, digestmod=knockotpd_config.algorithm)
		subprocess.run(knockotpd_config.nft + ['add', 'element', knockotpd_config.nft_table_type, knockotpd_config.nft_table_name, knockotpd_config.nft_table_set, '{', f'0x{otp.hexdigest()}', 'timeout', f'{knockotpd_config.interval + knockotpd_config.overlap}s', '}'])
