# knockotp

## SSH configuration

If you want to send the OTP automatically you can use the following SSH configuration:

```
Match host example.com exec "/path/to/knockotp.py %h"

# An IPv4 only host
Match host ipv4.example.com exec "/path/to/knockotp.py -4 %h"
	AddressFamily inet
```

# knockotpd

## nftable configuration

A typical nftable configuration for knockotpd to work with sha3-512 on port 22 would be:

```nft
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
	set authorized_ip {
		type ipv4_addr
		timeout 10s
	}

	set authorized_ip6 {
		type ipv6_addr
		timeout 10s
	}

	set otp {
		typeof @ih,0,512
		flags timeout
	}

	chain input {
		type filter hook input priority filter; policy drop;
		ct state established,related accept

		jump otp

		ip saddr @authorized_ip tcp dport 22 accept
		ip6 saddr @authorized_ip6 tcp dport 22 accept
		tcp dport 22 drop
	}

	chain otp {
		udp dport 22 @ih,0,512 @otp set add ip saddr @authorized_ip drop
		udp dport 22 @ih,0,512 @otp set add ip6 saddr @authorized_ip6 drop
	}

	chain forward {
		type filter hook forward priority filter; policy drop;
	}

	chain output {
		type filter hook output priority filter;
	}
}
```
